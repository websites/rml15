const now = (new Date).toISOString();
const fs = require("fs");
const yaml = require('js-yaml');
const md = require('marked');
md.setOptions({headerIds: false, xhtml: true});
const templatePages = {};
function importFile(path,fileName,templatePrefix=''){
  const baseName = fileName.split('.')[0];
  const ext = fileName.split('.')[1];
  const rawContent = fs.readFileSync(path+fileName, 'utf8');
  templatePages[templatePrefix+baseName] = ext==='md'?md(rawContent):rawContent;
}
fs.readdirSync('template/pages/').forEach((f)=>importFile('template/pages/',f));
fs.readdirSync('template/fragments/').forEach((f)=>importFile('template/fragments/',f,'#'));
templatePages["__programme"] = fs.readFileSync('generated.programme.html', 'utf8');
templatePages["__now"] = now;
function recursiveTemplateFill(templatePages){
  let changed = false;
  for(let key in templatePages){
    let value = templatePages[key];
    if(value.indexOf('{build:')!== -1){
      templatePages[key] = applyData2Template(templatePages,value);
      if (value !== templatePages[key]) changed = true;
      else console.error(`Template inconnu dans ${key}`);
    }
  }
  if(changed) recursiveTemplateFill(templatePages);
}
recursiveTemplateFill(templatePages);
replacesInFiles(["index.html","sitemap.xml", "humans.txt"],templatePages);

function replacesInFiles(fileList,buildReplaces) {
    const configReplaces = yml2replaceMap('event.config.yml');
    for(let fileName of fileList){
      let fileContent = fs.readFileSync(`template/${fileName}`,"utf8");

      fileContent = applyData2Template(buildReplaces,fileContent, 'build');
      fileContent = applyData2Template(configReplaces,fileContent,'conf');
      // anchors to SEO links
      fileContent = fileContent.replace(/="#([^"]*)"/g,(match,catched)=>`="./${catched?catched+'.html':''}"`);

      fs.writeFileSync(`generated.public/${fileName}`,fileContent,"utf8");
    }
}
function yml2replaceMap(dataFile){
  const data = yaml.safeLoad(fs.readFileSync(dataFile, 'utf8'));
  return flatTree(data);
}
function flatTree(obj) {
  if(typeof obj !== 'object') return obj;
  const res = {};
  for(let key in obj){
    const value = obj[key];
    if(typeof value !== 'object') res[key]=value;
    else for (let subKey in value) res[key+'.'+subKey]=flatTree(value[subKey]);
  }
  return res;
}
function applyData2Template(replaceData,templateStr,prefix='build') {
  const regex = new RegExp(Object.keys(replaceData).map(key=>`\\{${prefix}\\:${escapeRegex(key)}\\}`).join('|'),'g');
  const replacements = {};
  for (let k in replaceData) replacements[`{${prefix}:${k}}`]=replaceData[k];

  return templateStr.replace(regex,match=>replacements[match]);
}
function escapeRegex(s) {
  return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
};
