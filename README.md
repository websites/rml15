l'adaptation du contenu se limite à changer le contenu des dossiers et fichiers : 
- `programme/`
- `template/pages/`
- `template/index.html`
- `event.config.yml`

Et potentiellement changer quelques contenu dans `static/img/`

Le reste devrait rouler avec : 
- pour tester en local : après install de node.js et npm : `npm install` suivi de `npm run watch` dans le dossier du projet.
- pour mettre en prod : un push sur un dépot pour le quel j'ai configuré la bonne CI/CD

# RML 15
- Site accessible à l'adresse : https://rml15.axiom-team.fr/
- Urls de secours : https://rml15.monnaie-libre.fr/
- Urls de secours (fonctionnelle dès maintenant) : https://rml15.duniter.io/

# Tutoriel pour débutants

## Mise en place

- cloner le dépôt git avec 
```
git clone https://framagit.org/1000i100/rml15.git
```
- installer les outils requis (nodejs et npm) avec par exemple 
```
sudo apt install npm nodejs
```
- aller dans le répertoire rml15 et installer les dépendances npm
```
npm install
```
- lancer la compilation du site avec
```
npm run watch
```
- servir le site par exemple avec python (rafraîchir pour actualiser)
```
python3 -m http.server
```
- se rendre à l'adresse http://localhost:8000/

## Développement

Les fichiers à modifier sont principalement dans le dossier `programme`. Il s'agit des fichiers suivants :
- `speakers.yml` qui contient une liste d'intervenants et des informations relatives à eux
- `places.yml` qui contient une liste de lieux et des information relatives à ceux-ci
- `events.yml` qui contient une liste d'événements et leur détail
- `schedule.yml` qui contient le détail du déroulé des journées et fait référence aux informations présentes dans les fichiers ci-dessus
- vous pouvez également ajouter des images ou autre fichiers dans le répertoire `static`

Attention, si vos modifications introduisent des erreurs, ce sera visible dans le terminal dans lequel vous avez lancé `npm run watch`.
Ces erreurs ne sont pas forcément faciles à débugger, c'est pourquoi il vous faut faire attention aux points suivants :
- l'indentation est importante (le nombre de tabulation qui précède une ligne)
- les `uid` sont des identifiants uniquer (unique id) qui permettent de référencer un contenu d'un point à l'autre des fichiers. Cet identifiant doit rester unique et il faut s'y référer sans faute de frappe
- 

Une fois que vos modifications sont faites, il faut les "commiter" avec :
```bash
git add . # ajoute tous les fichiers non suivis (y compris les images par exemple)
git commit -m "résumé de vos modifications" # ajoute un bloc à la chaîne de git locale
git push # publie vos modifications vers le chaine git distante (il vous faudra vous identifier avec les identifiants framagit)
```
n'hésitez pas à faire relire vos modifications si vous avez un doute sur leur validité.