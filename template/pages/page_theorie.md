# Principes théoriques

## Principe de Relativité
Inspiré par le principe de relativité d'Albert Einstein et la spécificité de la vitesse de la lumière comme invariant,
l'auteur de la Théorie Relative de la Monnaie (TRM) nous propose un référentiel monétaire invariant :
le Dividende Universel (DU).
Il s'agit de cette création monétaire régulière, proportionnelle à la masse monétaire totale,
répartie à parts égales entre chaque individu tel que détaillé dans la 
[page traitant d'égalité et de symétrie](#symetrie_DU). 

L'analogie à la relativité et à l'invariance de la lumière peut être développée :

Tout comme la vitesse de deux objets peut être perçue différement selon l'observateur,
la valeur de deux objets peut être perçue différemment selon l'observateur.<br/>
Pour autant, la vitesse de la lumière reste invariante quel que soit l'observateur,
à l'identique, la valeur du DU est conçue pour rester invariante quel que soit l'observateur.

Pour approfondir : [Principe de relativité dans la TRM](http://trm.creationmonetaire.info/formalisaton.html#principe-de-relativite)

Ou en vidéo : [TRM : Quantitatif et Relatif, les 4 référentiels](https://www.youtube.com/watch?v=ZASkL9vmXSU)


## Symétrie Spatiale & Symétrie Temporelle
Qu'est-ce qui permet un invariant économique, autrement dit une unité de mesure égale pour tous ?
Mettre tout le monde à égalité dans son rapport à la monnaie.

C'est ce qui a été développé sur [la page traitant d'égalité et de symétrie spatiale et temporelle](#symetrie_DU)  

## Les quatre libertés :

- [Liberté 0 : L’individu est libre du choix de son système monétaire & de sa modification démocratique](#adoption_citoyenne)
- Liberté 1 : L’individu est libre d’accéder aux ressources & de les utiliser
- Liberté 2 : L’individu est libre d'estimer & de produire toute valeur économique
- Liberté 3 : L’individu est libre d’échanger, de comptabiliser & d'afficher ses prix « dans la monnaie »

## Pour approfondir : 
- [Appendice 1 de la TRM : Commentaires sur les quatre libertés économiques](http://trm.creationmonetaire.info/appendice-1.html)
- [Video : La Théorie Relative de la Monnaie par son auteur, Stéphane Laborde (2014)](https://youtu.be/PdSEpQ8ZtY4)
- [Video : 9/10 libérer la monnaie, Le 4ème singe](https://www.youtube.com/watch?v=tyxxRemLVgk)
- [Film complet : Oui au libre marché ! Libérons les économies, Le 4ème singe](https://www.4emesinge.com/oui-au-libre-marche-liberons-les-economies-le-film/)
- [Livre : La TRM en détails](http://monnaie.ploc.be/)
