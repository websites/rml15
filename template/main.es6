const baseUrl = location.href.substring(0,location.href.lastIndexOf('/')+1);

import '../node_modules/jparticles/production/jparticles';
import '../node_modules/jparticles/production/particle';

function runJS(){

try {
  new JParticles.particle('#particles',{
    opacity: .8,
    //resize: false,
    color: ['rgba(255,255,255,.2)', 'rgba(255,255,255,.4)', 'rgba(255,255,255,.5)', 'rgba(255,255,255,.6)','rgba(255,255,255,.8)','#fff', '#7af', '#ad0','#d84'],
    num: .25,
    range: 0.99,
    //range: 300,
    //proximity: 0.04,
    proximity: 100,
    maxSpeed: 0.1,
    minSpeed: 0.01,
    eventElem: document.getElementById("splash")
    /*	,parallaxLayer: [1, 2, 3, 0],
		  parallaxStrength: 30,
		  parallax: true
	  */
  });
} catch (e) {
  console.warn(e)
}

function idNodesExtractor(dom = document) {
	const subPages = document.querySelectorAll("[id]");
	const pagesArray = Object.keys(subPages).map((k)=>subPages[k]);
	return pagesArray.filter(n=>n.id.indexOf('-') === -1);
}
try {
idNodesExtractor().forEach(l=>{
	const subTitle = l.querySelector('h1,h2,h3,h4,h5,h6');
	if(!subTitle) return;
	if(subTitle.innerHTML === subTitle.textContent) subTitle.innerHTML = `<a href="./${l.id}.html" data-print="${baseUrl}${l.id}.html" class="permalien" title="permalien">${subTitle.innerHTML} </a>`;
	const link = document.createElement("a");
	link.classList.add(`anchorLink`);
	link.href = `./${l.id}.html`;
	link.title="permalien";
	subTitle.appendChild(link);
});
} catch (e) { console.warn(e) }
function reagenceParticiperForPrint(){
	const programme = document.getElementById('programme');
	const participerScreen = document.getElementById('participer');
	if(!programme || !participerScreen) return;
	const participerPrint = participerScreen.cloneNode(true);
	participerScreen.classList.add('neverPrint');
	participerPrint.classList.add('onlyPrint');
	participerPrint.id = 'participerPrint';
	programme.parentNode.insertBefore(participerPrint, programme.nextSibling);
}
try{
reagenceParticiperForPrint();
} catch (e) { console.warn(e) }

let nextScroll;
const baseNode = document.createElement("base");
baseNode.setAttribute("href",baseUrl);
document.head.appendChild(baseNode);

function scrollTo(dest, duration = 500) {
	const frequence = 10;
	if(window.getComputedStyle(dest).position === 'fixed') return;
	const to = Math.max(0,Math.min(pageOffsetTop(dest)-66, document.body.clientHeight - window.innerHeight));
	let timeLeft = duration;
	let last = Date.now() - frequence;
	nextScroll = () => {
		if ((document.body.scrollTop || document.documentElement.scrollTop || 0) === to) {
			nextScroll = () => 0;
			return;
		}
		const now = Date.now();
		const elapsed = now-last;
		last = now;
		let currPos = window.pageYOffset + (to - window.pageYOffset) / (timeLeft / elapsed);
		timeLeft -= elapsed;
		if(timeLeft<0) currPos = window.pageYOffset + (to - window.pageYOffset);
		document.body.scrollTop = document.documentElement.scrollTop = currPos;
		if(timeLeft>0) setTimeout(nextScroll, frequence);
	};
	nextScroll();
}

function pageOffsetTop(node){
	return node.offsetTop + (node.offsetParent? pageOffsetTop(node.offsetParent):0);
}

function highlight(target) {
	target.classList.add("highlight");
	setTimeout(() => target.classList.remove("highlight"), 500);
}
function bgCloseBuilder(side){
	const bgClose = document.createElement("a");
	bgClose.classList.add(`bg${side}`);
	bgClose.classList.add("bgPopupClose");
	bgClose.href = "./#";
	bgClose.title = "Fermer";
	return bgClose;
}
try{
document.querySelectorAll('.subPage').forEach( popup => {
	const fermer = document.createElement("a");
	fermer.classList.add("close");
	fermer.href = "./#";
	fermer.title = "Fermer";
	fermer.innerHTML = "X";
  popup.insertBefore(fermer, popup.firstChild);
  popup.insertBefore(bgCloseBuilder('Top'), popup.firstChild);
  popup.insertBefore(bgCloseBuilder('Left'), popup.firstChild);
  popup.insertBefore(bgCloseBuilder('Right'), popup.firstChild);
  popup.insertBefore(bgCloseBuilder('Bottom'), popup.firstChild);
} );
} catch (e) { console.warn(e) }

const baseTitle = document.querySelector('meta[property="og:site_name"]').content;
const fullBaseTitle = ""+document.title;
function getTitle(hash){
	let title;
	if(hash && hash !== 'top'){
		const target = document.getElementById(hash);
		const subTitle = target.querySelector('h1,h2,h3,h4,h5,h6');
		title = `${subTitle?subTitle.innerText:'Accueil'} ~ ${baseTitle}`;
	} else {
		title = fullBaseTitle;
	}
	return title;
}
try{
document.querySelectorAll('nav').forEach(nav=>nav.addEventListener('click',()=>document.querySelector('nav').classList.toggle("active")));
} catch (e) { console.warn(e) }
try{
let debutEasterEgg;
const easterEgg = () => {
  const fin = Date.now();
  if(fin-debutEasterEgg>5000) document.querySelectorAll('.disclaimer').forEach(n=>n.classList.add("ddisclaimer"));
};
document.querySelectorAll('.disclaimer').forEach(n=>n.addEventListener('mousedown',()=>{
  debutEasterEgg = Date.now();
  n.removeEventListener('mouseup',easterEgg);
  n.addEventListener('mouseup',easterEgg);
}));
} catch (e) { console.warn(e) }

document.querySelectorAll('a[href^="./"], a[href^="./"] *, a[href^="#"], a[href^="#"] *').forEach(internalNavLinking);
function internalNavLinking(linkNode) {
  linkNode.addEventListener('click',
    e => {
      if(!e.ctrlKey && !e.shiftKey && !e.altKey && !e.metaKey){
        e.preventDefault();
        e.stopPropagation();
        //const link = ancestorMatching((m)=>m.href,e.target);
        const link = e.target.closest('[href]');
        const hash = getHash(link.href);
        document.title = getTitle(hash);
        history.pushState({},document.title,hash?baseUrl+hash+'.html':baseUrl);
        sessionNav();
      }
    }
  )
}
function ancestorMatching(matcher, domNode) {
	return matcher(domNode) ? domNode : ancestorMatching(matcher, domNode.parentNode);
}
function getHash(href = location.href){
	let hash = href.split('#')[1];
	if(hash) return hash;
	const currentPathMarker = href.lastIndexOf('/')+1;
	if(currentPathMarker === href.length) return '';
	const extPositionMarker = href.lastIndexOf('.');
	if(currentPathMarker>extPositionMarker) return '';
	return href.substring(currentPathMarker,extPositionMarker);
}
try{
window.addEventListener('popstate',sessionNav);
sessionNav();
} catch (e) { console.warn(e) }

function sessionNav(){
	document.querySelectorAll('.subPage').forEach( section => section.classList.add("inactive") );
	document.querySelectorAll(`.subPage.active`).forEach( link => link.classList.remove("active") );
	document.body.classList.remove("starter");
	document.body.classList.remove("noScroll");
	scrollBarShift(0);
	const widthWithScrollBar = document.body.offsetWidth;
	let hash = getHash();
	if(hash === "top") {
		scrollTo(document.body);
		hash = '';
	}
	document.title = getTitle(hash);
	history.replaceState({},document.title,hash?baseUrl+hash+'.html':baseUrl);
	if(hash){
		const activeTarget = document.getElementById(hash);
		activeTarget.classList.add("active");
		activeTarget.classList.remove("inactive");
		document.querySelectorAll(`a[href="#${hash}"]`).forEach(ref=>ref.classList.add("active") );
		if(activeTarget.classList.contains("subPage")){
			document.body.classList.add("noScroll");
			const widthNoScrollBar = document.body.offsetWidth;
			scrollBarShift(widthNoScrollBar-widthWithScrollBar);
			if(window.pageYOffset < window.innerHeight) document.body.classList.add("starter");
		} else {
			scrollTo(activeTarget);
			highlight(activeTarget);
		}
	}
	optiPrint(hash);
}
function optiPrint(hash) {
	document.querySelectorAll('.noPrint').forEach(n=>n.classList.remove('noPrint'));
	document.querySelectorAll('.print').forEach(n=>n.classList.remove('print'));
	if(!hash) return;
	const page = document.getElementById(hash);
	let ancestor = page;
	while (ancestor.parentNode){
		brothers(ancestor).forEach(n=>n.classList.add('noPrint'));
		ancestor = ancestor.parentNode;
	}
	if(hash === 'decouvrir'){
		page.querySelectorAll('.subPage').forEach(n=>n.classList.add('print'));
	}
}
function brothers(node) {
	const parent = node.parentNode;
	if(parent === node) return [];
	const bro = [];
	for(let i=0;i<parent.children.length;i++){
		if(parent.children[i] !== node) bro.push(parent.children[i]);
	}
	return bro;
}
function scrollBarShift(size){
	document.querySelectorAll("nav, body>section").forEach(e => e.style.paddingRight = size+"px");
	document.querySelectorAll("body>aside").forEach(e => e.style.right = size+"px");
}



function getParentChildIndexOf(node){
	let child = node;
	let childIndex = 0;
	while( child.previousElementSibling != null ){
		child = child.previousElementSibling;
		childIndex++;
	}
	return childIndex;
}
try{
document.querySelectorAll('.large thead th').forEach( column => {
	column.addEventListener("click",e => {
		const childIndex = getParentChildIndexOf(e.target);
		document.querySelectorAll(`.large tr`).forEach( tr => {
			const node = tr.children[childIndex];
			node.classList.toggle("compact");
			if(!node.querySelectorAll(".short").length) node.innerHTML = `<span class="short"></span><span class="long">${node.innerHTML}</span>`;
		} );
	} );
} );
} catch (e) { console.warn(e) }
try{
	document.querySelectorAll('.session>.subPageLink, .timeBloc').forEach( programmLink => {
		const dayNode = programmLink.closest('li.day');
		const daySpeakerClasses = dayNode.className.split(' ').filter(cl=>cl.indexOf('speaker_')!==-1);
		const roomClass = programmLink.closest('[class*="room_"]')?'room_'+programmLink.closest('[class*="room_"]').className.split('room_')[1].split(' ')[0]:'null';
		const speakerClasses = programmLink.closest('[class*="speaker_"]')?programmLink.closest('[class*="speaker_"]').className.split(' ').filter(cl=>cl.indexOf('speaker_')!==-1):['null'];
		programmLink.addEventListener("mouseenter",e => {
			dayNode.querySelectorAll('.timeBloc, .session>.subPageLink, .roomRef').forEach(elem=>elem.classList.remove('hover'));
			dayNode.querySelectorAll(`[href="./${getHash(e.target.href)}.html"]`).forEach(elem=>elem.classList.add('hover'));
			dayNode.querySelectorAll(`.roomRef.${roomClass}`).forEach(elem=>elem.classList.add('hover'));
			document.querySelectorAll('.speakerHead').forEach(elem=>elem.classList.add('unselected'));
			document.querySelectorAll(speakerClasses.map(spCl=>`.speakerHead.${spCl}`).join(',')).forEach(elem=>elem.classList.remove('unselected'));
		});
		programmLink.addEventListener("mouseleave",() => {
			dayNode.querySelectorAll('.timeBloc, .session>.subPageLink, .roomRef').forEach(elem=>elem.classList.remove('hover'));
			document.querySelectorAll('.speakerHead').forEach(elem=>elem.classList.add('unselected'));
			document.querySelectorAll(daySpeakerClasses.map(spCl=>`.speakerHead.${spCl}`).join(',')).forEach(elem=>elem.classList.remove('unselected'));
		});
	});
} catch (e) { console.warn(e) }
try{
	document.querySelectorAll('li.day').forEach( day => {
		const speakerClasses = day.className.split(' ').filter(cl=>cl.indexOf('speaker_')!==-1);
		day.addEventListener("mouseenter",e => {
			document.querySelectorAll('.speakerHead').forEach(elem=>elem.classList.add('unselected'));
			document.querySelectorAll(speakerClasses.map(spCl=>`.speakerHead.${spCl}`).join(',')).forEach(elem=>elem.classList.remove('unselected'));
		});
		day.addEventListener("mouseleave",() => {
			document.querySelectorAll('.speakerHead').forEach(elem=>elem.classList.remove('unselected'));
		});
	});
} catch (e) { console.warn(e) }
try{
document.querySelectorAll('.speakerHead').forEach( speakerLink => {
	const speakerClass = 'speaker_'+speakerLink.className.split('speaker_')[1].split(' ')[0];
	speakerLink.addEventListener("mouseenter",() => {
		document.querySelectorAll('.session>.subPageLink, .speakerHead').forEach(elem=>elem.classList.add('unselected'));
		document.querySelectorAll(`.${speakerClass}`).forEach(elem=>elem.classList.remove('unselected'));
	});
	speakerLink.addEventListener("mouseleave",() => {
		document.querySelectorAll('.session>.subPageLink, .speakerHead').forEach(elem=>elem.classList.remove('unselected'));
	});
	speakerLink.addEventListener("click",() => {
		if(document.querySelector(`.speakerHead.${speakerClass}`).classList.contains('active')){
			document.querySelectorAll('.day, .timeBloc, .session>.subPageLink, .speakerHead').forEach(elem=>elem.classList.remove('inactive', 'active'));
		} else {
			document.querySelectorAll('.day, .timeBloc, .session>.subPageLink, .speakerHead').forEach(elem=>{
				elem.classList.add('inactive');
				elem.classList.remove('active');
			});
			document.querySelectorAll(`.${speakerClass}`).forEach(elem=>{
				elem.classList.remove('inactive');
				elem.classList.add('active');
				const day = elem.closest('.day');
				if(day) day.classList.remove('inactive');
			});
			const hash = "programme";
			//document.title = getTitle(hash);
			//history.replaceState({},document.title,hash?baseUrl+hash+'.html':baseUrl);
			//sessionNav();
			const target = document.getElementById(hash);
			scrollTo(target);

		}
	});
});
} catch (e) { console.warn(e) }
try{
document.querySelectorAll(".videoArea").forEach(n=>{
  n.addEventListener('click',(e)=>{
    e.preventDefault();
    embedVideo(n.parentNode);
  });
});
} catch (e) { console.warn(e) }

function embedVideo(videoNode){
  const vArea = videoNode.querySelector('.videoArea');
  const vMirrors = videoNode.querySelector('.mirrors');
  videoNode.innerHTML = `<iframe width="${vArea.offsetWidth}" height="${vArea.offsetHeight}" sandbox="allow-same-origin allow-scripts" src="${vArea.href.replace("/videos/watch/", "/videos/embed/")}?autoplay=true" frameborder="0" allowfullscreen></iframe>`;
  videoNode.appendChild(vMirrors);
}

document.querySelectorAll(".timeline").forEach(n=>n.style.height = `${n.offsetHeight}px`);

function getDateTimeFromMeta(keyword = 'start'){
  const date = document.querySelector(`meta[name="for_js:${keyword}_date"]`).content.split('-');
  const time = document.querySelector(`meta[name="for_js:${keyword}_time"]`).content.split('h');
  return new Date(date[0],date[1],date[2],time[0],time[1],0);
  }
try{
const countdownNode = document.querySelector(".countdown");
const countdown = ()=> {
	const eventStart = getDateTimeFromMeta('start').getTime();
	const eventEnd = getDateTimeFromMeta('end').getTime();
	let started = false;
	let timeLeft = Math.max(0, Math.round((eventStart - Date.now())/1000));
	if(!timeLeft) {
		started = true;
		timeLeft = Math.max(0, Math.round((eventEnd - Date.now())/1000));
	}
	const daysLeft = Math.floor(timeLeft/3600/24);
	const hoursLeft = Math.floor( (timeLeft-(daysLeft*3600*24))/3600);
	const minutesLeft = Math.floor( (timeLeft-Math.floor(timeLeft/3600)*3600)/60);
	const secondsLeft = Math.floor( (timeLeft-Math.floor(timeLeft/60)*60));
	let res = 'Dans ';
	if(started){
		res = 'Encore ';
		if(daysLeft) res+=` ${daysLeft}&nbsp;jour${daysLeft>1?'s':''}`;
		else{
			if(hoursLeft) res+=` ${hoursLeft}&nbsp;heure${hoursLeft>1?'s':''}`;
			if(minutesLeft) res+=` ${minutesLeft}&nbsp;minute${minutesLeft>1?'s':''}`;
			if(secondsLeft) res+=` ${secondsLeft}&nbsp;seconde${secondsLeft>1?'s':''}`;
		}
		res += '. Profitez-en&nbsp;!';
	} else{
		if(daysLeft) res+=` ${daysLeft}&nbsp;jour${daysLeft>1?'s':''}`;
		if(hoursLeft) res+=` ${hoursLeft}&nbsp;heure${hoursLeft>1?'s':''}`;
		if(minutesLeft) res+=` ${minutesLeft}&nbsp;minute${minutesLeft>1?'s':''}`;
		if(secondsLeft) res+=` ${secondsLeft}&nbsp;seconde${secondsLeft>1?'s':''}`;
	}
	if(timeLeft) countdownNode.innerHTML = `<span class="neverPrint">${res}</span><span class="onlyPrint">Rencontres Monnaie Libre<br/><span style="font-size: 80%">douzième édition</span></span>`;
	else {
    clearInterval(countdownHolder);
    if (document.querySelectorAll(".videoArea").length) {
      countdownNode.innerHTML = `Les premières vidéos sont disponibles : <div class="vids">${
        [...document.querySelectorAll(".videoArea")].map((n, i) => `
      <a href="./${n.closest('.subPage').id}.html"><img src="img/picto/rec/video.svg" alt="📹"/></a>`).filter((n, i, a) => a.indexOf(n) >= i).join(' ')
        }</div>`;
      [...document.querySelectorAll(".vids a")].forEach(internalNavLinking);
    } else {
      countdownNode.innerHTML = "Merci à tous&nbsp! Bientôt les vidéos.";
    }
  }
}
const countdownHolder = setInterval(countdown, 1000);
} catch (e) { console.warn(e) }

  function extractTime(node,attr){
    return (new Date(node.getAttribute(attr))).getTime();
  }
try{
  document.querySelectorAll(".record[data-start-live]").forEach(n=>{
    if(extractTime(n,"data-start-live")>Date.now()){
      //n.querySelector('img').src="img/picto/rec/will_be_recorded.svg";
      //n.querySelector('.infoBox').innerHTML="Sera diffusé en live puis disponible sur Peertube.";
    } else if(extractTime(n,"data-end-live")>Date.now()){
      n.querySelector('img').src="img/picto/rec/live.svg";
      n.querySelector('.infoBox').innerHTML="Actuellement en direct !";
    }
  });
} catch (e) { console.warn(e) }


}
window.runJS = runJS;
